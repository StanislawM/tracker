import { Vacation } from './../model/vacation';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class VacationService {
  private url = '/api/vacations';

  constructor(private httpClient: HttpClient) {
  }

  public static emptyVacation() {
    return {
      startDate: "",
      endDate: "",
      totalDuration: "",
      date: new Date,
      type: ""
    };
  }

  public getVacationList(): Observable<Vacation[]> {
    return this.httpClient.get<Vacation[]>(this.url + '?_expand=user');
  }

  public getVacationById(id: string): Observable<Vacation> {
    return this.httpClient.get<Vacation>(this.url + `/${id}`);
  }

  public createVacation(vacation: Vacation): Observable<Vacation> {
    return this.httpClient.post<Vacation>(this.url, vacation);
  }

  public updateVacation(id: string, vacation: Vacation): Observable<Vacation> {
    return this.httpClient.put<Vacation>(this.url + `/${id}`, vacation);
  }

  public deleteVacation(id: string): Observable<Vacation> {
    return this.httpClient.delete<Vacation>(this.url + `/${id}`);
  }
}
