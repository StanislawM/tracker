import { User } from './../model/user';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private url = '/api/users';

  constructor(private httpClient: HttpClient) {
  }

  public static emptyUser() {
    return {
      key: "",
      password: "",
      name: "",
      surname: "",
      startDate: new Date,
      birthday: "",
      gender: "",
      martialStatus: "",
      role: "",
      mobileNumber: "",
      status: ""
    };
  }

  public getUserList(): Observable<User[]> {
    return this.httpClient.get<User[]>(this.url + '?_embed=vacations');
  }

  public getUserById(id: string): Observable<User> {
    return this.httpClient.get<User>(this.url + `/${id}`);
  }

  public createUser(user: User): Observable<User> {
    return this.httpClient.post<User>(this.url, user);
  }

  public updateUser(id: string, user: User): Observable<User> {
    return this.httpClient.put<User>(this.url + `/${id}`, user);
  }

  public deleteUser(id: string): Observable<User> {
    return this.httpClient.delete<User>(this.url + `/${id}`);
  }
}
