import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Settings } from '../model/settings';

@Injectable({
  providedIn: 'root',
})
export class SettingsService {
  private url = '/api/settings';

  constructor(private httpClient: HttpClient) {
  }

  public getSettingsByName(name: string): Observable<Settings> {
    return this.httpClient.get<Settings>(`${this.url}?name=${name}`);
  }
}
