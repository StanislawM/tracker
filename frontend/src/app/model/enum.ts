export enum VacationType {
  ANNUAL,
  SICK_DAY,
  UNPAID,
  PATERNAL,
  MATERNITY
}

export enum Gender {
  MALE,
  FEMALE,
  INTERSEX
}

export enum MartialStatus {
  MARRIE,
  WIDOWED,
  SEPARATED,
  DIVORCED,
  SINGLE
}

export enum Role {
  EMPLOYEE,
  ADMIN
}

export enum UserStatus {
  ACTIVE = 1,
  INACTIVE = 0
}
