import { VacationType } from './enum';
import { User } from "./user";

export interface Vacation {
  id: string;
  startDate: string;
  endDate: string;
  totalDuration: number;
  date: Date;
  type: VacationType;
  user: User;
  userId: number;
}
