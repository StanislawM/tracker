import { Gender, MartialStatus, Role, UserStatus } from "./enum";
import { Vacation } from "./vacation";

export interface User {
  id: string;
  key: string;
  password: string;
  name: string;
  surname: string;
  startDate: string;
  birthday: string;
  gender: Gender;
  martialStatus: MartialStatus;
  role: Role;
  mobileNumber: number;
  status: UserStatus;
  vacations?: Vacation[];
}
