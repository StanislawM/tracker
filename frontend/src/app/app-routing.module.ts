import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserListComponent } from './components/user-list/user-list.component';
import { UserDetailsComponent } from './components/user-details/user-details.component';
import { VacationListComponent } from './components/vacation-list/vacation-list.component';
import { VacationDetailsComponent } from './components/vacation-details/vacation-details.component';
import { StatisticsComponent } from "./components/statistics/statistics.component";

const routes: Routes = [
  {path: '', redirectTo: 'user', pathMatch: 'full'},
  {path: 'user', component: UserListComponent},
  {path: 'user/:id', component: UserDetailsComponent},
  {path: 'vacation', component: VacationListComponent},
  {path: 'statistics', component: StatisticsComponent},
  {path: 'vacation/:id', component: VacationDetailsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
