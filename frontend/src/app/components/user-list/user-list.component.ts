import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/model/user';
import { UserService } from 'src/app/service/user.service';
import { MatDialog } from "@angular/material/dialog";
import { UserDetailsComponent } from "../user-details/user-details.component";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent implements OnInit {
  public dataSource: User[] = [];
  public displayedColumns: string[] = [
    'name',
    'surname',
    'startDate',
    'role',
    'status',
    'action',
  ];

  constructor(
    private userService: UserService,
    public dialog: MatDialog
  ) {
  }

  ngOnInit(): void {
    this.getUserList();
  }

  createNewUser(): void {
    const dialogRef = this.dialog.open(UserDetailsComponent, {
      width: '65%',
      data: UserService.emptyUser(),
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.formatBeforeMerge(result);
        this.userService.createUser(result).subscribe(() => this.getUserList());
      }
    });
  }

  editUser(element: User) {
    const dialogRef = this.dialog.open(UserDetailsComponent, {
      width: '65%',
      data: element,
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.formatBeforeMerge(result);
        this.userService.updateUser(result.id, result).subscribe(() => this.getUserList());
      }
    });
  }

  deleteUser(element: User) {
    this.userService.deleteUser(element.id).subscribe(() => this.getUserList());
  }

  private getUserList() {
    this.userService.getUserList().subscribe((date) => {
      this.dataSource = date;
    });
  }

  private formatBeforeMerge(result: any) {
    result.startDate = result.startDate.toLocaleDateString()
    result.birthday = result.birthday.toLocaleDateString()
  }
}
