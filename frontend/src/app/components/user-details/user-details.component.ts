import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { User } from "../../model/user";
import { Gender, MartialStatus } from "../../model/enum";

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss'],
})
export class UserDetailsComponent {
  MartialStatus = Object.keys(MartialStatus).filter(v => isNaN(Number(v)));
  Gender = Object.keys(Gender).filter(v => isNaN(Number(v)));

  constructor(
    public dialogRef: MatDialogRef<UserDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: User
  ) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
