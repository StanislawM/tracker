import { Component, OnInit } from '@angular/core';
import { Vacation } from "../../model/vacation";
import { MatDialog } from "@angular/material/dialog";
import { VacationDetailsComponent } from "../vacation-details/vacation-details.component";
import { VacationService } from "../../service/vacation.service";

@Component({
  selector: 'app-vacation-list',
  templateUrl: './vacation-list.component.html',
  styleUrls: ['./vacation-list.component.scss']
})
export class VacationListComponent implements OnInit {
  public dataSource: Vacation[] = [];
  public displayedColumns: string[] = [
    'startDate',
    'endDate',
    'totalDuration',
    'type',
    'user',
    'action',
  ];

  constructor(
    private vacationService: VacationService,
    public dialog: MatDialog
  ) {
  }

  ngOnInit(): void {
    this.getVacationList();
  }

  createNewVacation(): void {
    const dialogRef = this.dialog.open(VacationDetailsComponent, {
      width: '65%',
      data: VacationService.emptyVacation(),
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.formatBeforeMerge(result);
        this.vacationService.createVacation(result).subscribe(() => this.getVacationList());
      }
    });
  }

  deleteVacation(element: Vacation) {
    this.vacationService.deleteVacation(element.id).subscribe(() => this.getVacationList());
  }

  private getVacationList() {
    this.vacationService.getVacationList().subscribe((date) => {
      this.dataSource = date;
    });
  }

  private formatBeforeMerge(result: any) {
    result.totalDuration = Math.abs(result.startDate.getTime() - result.endDate.getTime()) / 3600000 + 24;
    result.startDate = result.startDate.toLocaleDateString()
    result.endDate = result.endDate.toLocaleDateString()
  }
}
