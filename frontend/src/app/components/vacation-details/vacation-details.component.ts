import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { Vacation } from "../../model/vacation";
import { VacationType } from "../../model/enum";
import { User } from "../../model/user";
import { UserService } from "../../service/user.service";

@Component({
  selector: 'app-vacation-details',
  templateUrl: './vacation-details.component.html',
  styleUrls: ['./vacation-details.component.scss']
})
export class VacationDetailsComponent implements OnInit {
  VacationType = Object.keys(VacationType).filter(v => isNaN(Number(v)));
  users: User[] = [];
  range: any;

  constructor(
    private userService: UserService,
    public dialogRef: MatDialogRef<VacationDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Vacation
  ) {
  }

  ngOnInit(): void {
    this.userService.getUserList().subscribe((users) => {
      this.users = users;
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
