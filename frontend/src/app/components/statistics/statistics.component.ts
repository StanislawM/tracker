import { Component, ElementRef, ViewChild } from '@angular/core';
import * as d3 from 'd3';
import { ScaleBand } from 'd3';
import { UserService } from "../../service/user.service";
import { User } from "../../model/user";

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent {
  data: { name: string, series: { name: string, value: number }[] }[] = [];
  domain = [0, 100];
  height = 300;
  innerPadding = 0.1;
  outerPadding = 0.1;
  seriesInnerPadding = 0.1;
  barColors = ['#00aeef', '#f98e2b', '#7C77AD'];

  public svg!: d3.Selection<SVGGElement, unknown, null, undefined>;
  public isRendered = false;

  @ViewChild('svgContainer', {read: ElementRef, static: true}) svgContainerRef!: ElementRef<HTMLDivElement>;

  constructor(private userService: UserService) {
    this.getData();
  }

  private getData() {
    this.data = [];
    this.userService.getUserList().subscribe((users) => {
      this.collectDataFromUser(users);
      this.createChart();
    });
  }

  private collectDataFromUser(users: User[]) {
    users.forEach(user => {
      const name = user.surname + ' ' + user.name;
      let series: { name: string; value: number; }[] = [];
      this.getSeries(user, series);
      this.data.push({name, series})
    })
  }

  private getSeries(user: User, series: { name: string; value: number }[]) {
    user.vacations?.forEach(vacation => {
      series.push({name: vacation.startDate, value: vacation.totalDuration})
    })
  }

  private createSVG(): void {
    this.svg = d3.select(this.svgContainerRef.nativeElement)
      .append('svg')
      .attr('width', '100%')
      .attr('height', this.height)
      .append('g');
  }

  private isDataValid(): boolean {
    return this.data && this.data.length > 0;
  }

  private getBandScale(domain: string[], range: any, innerPadding = 0, outerPadding = 0) {
    const scale: any | ScaleBand<string> = d3.scaleBand()
      .range(range)
      .domain(domain)
      .paddingInner(innerPadding)
      .paddingOuter(outerPadding);
    scale.type = 'BAND';
    return scale;
  }

  private createChart(): void {
    if (!this.isRendered) {
      this.createSVG();
    }
    if (this.isDataValid()) {
      const margin = {top: 10, left: 50, right: 10, bottom: 20};

      let height = this.height - margin.top - margin.bottom;
      const width = this.svgContainerRef.nativeElement.getBoundingClientRect().width - margin.left - margin.right;
      const groupNames = this.data.map(item => item.name);
      const groupLabels = this.data.length > 0 ? this.data[0].series.map(item => item.name) : [];

      const xScale = this.getBandScale(groupNames, [0, width], this.innerPadding, this.outerPadding).round(true);
      const x1Scale = this.getBandScale(groupLabels, [0, xScale.bandwidth()], this.seriesInnerPadding, this.outerPadding).round(true);

      let chartContainer = this.svg.selectAll<SVGGElement, number>('g.chart-container').data([1]);
      chartContainer = chartContainer.enter()
        .append('g')
        .attr('transform', `translate(${margin.left}, ${margin.right})`);

      let chartWrap = chartContainer.selectAll<SVGGElement, number>('g.chart-wrap').data([1]);
      chartWrap = chartWrap.enter().append('g');

      const xAxis = chartWrap.selectAll<SVGGElement, number>('g.x-axis').data([1]);
      xAxis.enter()
        .append('g')
        .merge(xAxis)
        .attr('transform', `translate(0, ${height})`)
        .call(d3.axisBottom(xScale)).selectAll('text')
        .style('text-anchor', 'middle');

      const y = d3.scaleLinear().domain(this.domain).nice().rangeRound([height, 0]);
      let yAxis = chartWrap.selectAll<SVGGElement, number>('g.y-axis').data([1]);
      yAxis.enter().append('g').merge(yAxis).call(d3.axisLeft(y));

      let barWrap = chartWrap.selectAll<SVGGElement, number>('g.bar-wrap').data([1]);
      barWrap = barWrap.enter().append('g').merge(barWrap);

      let barGroup = barWrap.selectAll<SVGGElement, { name: string, series: { name: string, value: number } }>('g.bar-group').data(this.data);
      barGroup = barGroup.enter().append('g').merge(barGroup)
        .attr('transform', d => `translate(${xScale(d.name)}, 0)`);

      let barRects = barGroup.selectAll<SVGRectElement, { name: string, value: number }>('rect.bar').data(d => d.series.map(item => item));
      barRects.enter()
        .append('rect')
        .merge(barRects)
        .attr('width', x1Scale.bandwidth())
        .attr('height', d => height - y(d.value))
        .attr('x', (d: any) => x1Scale(d.name))
        .attr('y', d => y(d.value))
        .attr('fill', (d, i) => this.barColors[i]);

    }
  }
}
