# Tracker

1. Run the command `npm install` in the "./tracker/frontend" directory.
2. Run the command `npm install -g json-server` (https://www.npmjs.com/package/json-server)
3. In directory "./tracker" run the command `json-server --watch backend.json`
4. In directory "./tracker/frontend" run the command `npm run start`